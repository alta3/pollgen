# Import necessary libraries
from flask import Flask, render_template, flash, request, redirect, url_for, session, make_response
import yaml
import io
from datetime import datetime
import os
import markdown
import json


# Define default values for configuration
default_labs_directory = "labs/courses"
default_performance_path = "labs/content/questions/performance.yml"
default_day_one_path = 'labs/content/questions/common-day1.yml'
default_port = 2224

# Initialize the Flask application
app = Flask(__name__)

# Set a secret key for session management
app.secret_key = "any random string"

# Retrieve configuration values from environment variables or use defaults
PORT = os.environ.get('POLLGEN_PORT', default_port)
LABS = os.environ.get('POLLGEN_LABS', default_labs_directory)
DAY_ONE = os.environ.get('POLLGEN_DAY_ONE', default_day_one_path)
PERFORMANCE = os.environ.get('POLLGEN_PERFORMANCE', default_performance_path)


# Function to list courses with questions
def list_courses_with_questions():
    try:
        course_names = []
        subdirectories = [item for item in os.listdir(LABS) if os.path.isdir(os.path.join(LABS, item))]
        for subdir in subdirectories:
            subdir_path = os.path.join(LABS, subdir)
            if 'questions' in os.listdir(subdir_path):
                course_names.append(subdir)
                course_names.sort()
        return course_names
    except FileNotFoundError as file_error:
        # Handle the case where the LABS directory path is invalid
        flash(f"Error in directories: {file_error}", "error")
        return []
    except Exception as e:
        # Handle other exceptions gracefully
        flash(f"Error in directories: {e}", "error")
        return []

# Initialize a dictionary to store questions with missing figure files
missing_figure_questions = {}

def get_course_questions(course_name):
    try:
        with open(f"{LABS}/{course_name}/questions/questions.yml", 'r') as file:
            course_questions = yaml.safe_load(file)

            # Check for missing figure files
            for question in course_questions:
                figure = question.get('figure', "")
                if figure != ""  and not os.path.exists(f"{LABS}/{course_name}/questions/{figure}"):
                    missing_figure_questions[question['question']] = question

            if not isinstance(course_questions, list):
                # Handle the case where course_questions is not a list
                flash("Course questions should be a list.", "error")
                return "ERROR"

            with open(PERFORMANCE, 'r') as file:
                performance = yaml.safe_load(file)
                if not isinstance(performance, list):
                    # Handle the case where performance is not a list
                    flash("Performance data should be a list.", "error")
                    return "ERROR"

                session['performance'] = performance

            return course_questions
    except FileNotFoundError as file_not_found_error:
        error_message = f"File not found: {file_not_found_error}"
        flash(error_message, "error")
        return "ERROR"
    except yaml.YAMLError as yaml_error:
        error_message = f"Error parsing YAML: {yaml_error}"
        flash(error_message, "error")
        return "ERROR"
    except ValueError as value_error:
        error_message = f"Error in file content: {value_error}"
        flash(error_message, "error")
        return "ERROR"



# Define the index route
@app.route('/')
def index():
   
    session['selected_day'] = 0
  
    session.pop('course_questions', None)
    return render_template('form.html', course_list=list_courses_with_questions())

# Define the route to process the form
@app.route('/process_form', methods=['POST'])
def process_form():
    
    # Retrieve the selected course from the form
    course_name = ""
    course_name = request.form['course']
    session['course_name'] = course_name
    return redirect(url_for('result', course_name=course_name))

# Define the route to display results
@app.route('/result/<course_name>', methods=["GET", "POST"])
def result(course_name):
            course_questions = get_course_questions(course_name) 
            if course_questions == "ERROR":
                return redirect('/')
            if course_questions is None:
                flash("There are no questions in the .yml file!", "error")
                return redirect('/')
            selected_day = session.get('selected_day')  # Retrieve selected day
            days = []
            for item in course_questions:
                if item['day'] not in days:
                    days.append(item['day'])
            performance_questions = session.get('performance')
            return render_template('choices.html', course=course_name, questions=course_questions, days=days, performance_questions=performance_questions, selected_day=selected_day, missing_figure_questions=missing_figure_questions)

@app.route('/result/<course_name>/select_day', methods=["POST"])
def select_day(course_name):
    selected_day = int(request.form.get('day'))
    session.pop('selected_day', 0)
    session['selected_day'] = selected_day
    
    return redirect(f"/result/{course_name}")

# baseurl / {course_name} / ?day=# / ?get-json=true


@app.route("/daily_performance")
def download_daily_performance():
        performance = session.get('performance')
        file_name = "Daily-Performance-Questions.json"
    
        json_data = {
            "pages": [
                {
                    "name": "Daily Performance Questions",
                    "elements": []
                }
            ]
        }
        for question in performance:
            if question['type'] == 'text':
                question_data = {
                    "type": "text",
                    "name": question["name"],
                    "title": question["question"],
                }
                json_data["pages"][0]["elements"].append(question_data)
            else:
                question_data = {
                    "type": "radiogroup",
                    "name": f"{question['name']}",
                    "title": question["question"],
                    "choices": [{"text": answer["text"], "correct": answer['correct'], "value": answer["value"]} for answer in question["answers"]]
                }
                json_data["pages"][0]["elements"].append(question_data)
        
        json_string = json.dumps(json_data, indent=4)

        # Create an in-memory file-like object
        file_stream = io.BytesIO(json_string.encode('utf-8'))

        # Create a response with the JSON content
        response = make_response(file_stream.getvalue())

        # Set the appropriate headers for JSON file download
        response.headers.set('Content-Type', 'application/json')
        response.headers.set('Content-Disposition', 'attachment', filename=file_name)

        return response
    
# Define the route to download Common Day 1 questions as JSON
@app.route("/day1_json")
def download_json():
        with open(DAY_ONE, 'r') as file:
            day1 = yaml.safe_load(file)
        # Create timestamp and filename
        today = datetime.today()
        formatted_date = today.strftime("%Y-%m-%d")
        file_name = f"generic-intro-questions-{formatted_date}.json"
    
        json_data = {
            "pages": [
                {
                    "name": f"generic_intro_questions_{formatted_date}",
                    "elements": []
                }
            ]
        }
        for question in day1:
            if question['type'] == 'text':
                question_data = {
                    "type": "text",
                    "name": f"{question['question']}",
                    "title": question["question"]
                }
                json_data["pages"][0]["elements"].append(question_data)
            else:
                question_data = {
                    "type": "radiogroup",
                    "name": f"{question['question']}",
                    "title": question["question"],
                    "choices": [{"text": answer["text"], "correct": answer['correct']} for answer in question["answers"]]
                }
                json_data["pages"][0]["elements"].append(question_data)
        
        json_string = json.dumps(json_data, indent=4)

        # Create an in-memory file-like object
        file_stream = io.BytesIO(json_string.encode('utf-8'))

        # Create a response with the JSON content
        response = make_response(file_stream.getvalue())

        # Set the appropriate headers for JSON file download
        response.headers.set('Content-Type', 'application/json')
        response.headers.set('Content-Disposition', 'attachment', filename=file_name)

        return response

# Define the route to generate JSON file for selected questions
@app.route('/json_download', methods=['POST'])
def create_json():
    selected_question_index = request.form.getlist('selected_question_index')
    selected_questions = []
    course_name = session.get('course_name')
    course_questions = get_course_questions(course_name)
    performance = session.get('performance')
    questions = performance + course_questions
    for index in selected_question_index:
        i = int(index)
        selected_questions.append(questions[i])     
    
    # Create timestamp and filename
    today = datetime.today()
    formatted_date = today.strftime("%Y-%m-%d")
    file_name = f"{course_name}-questions-{formatted_date}.json"
    
    json_data = {
        "pages": [
            {
                "name": f"{course_name}_{formatted_date}",
                "elements": []
            }
        ]
    }
  
    figure_questions = []

    if not selected_questions:
        flash("No questions selected, hit submit first!", "error")
        return redirect(url_for('result', course=course_name))
    else:
        for question in selected_questions:
            figure = question.get('figure')
            if figure:
                figure_questions.append(question)

    for question in selected_questions:
        if question not in figure_questions:
            if question['type'] == 'text':
                question_data = {
                    "type": "text",
                    "title": question["question"],
                    "isRequired": True,
                }
                if 'name' in question:
                    question_data["name"] = question['name']
                else:
                    question_data["name"] = question["question"]
                json_data["pages"][0]["elements"].append(question_data)
            else:
                question_data = {
                    "type": "radiogroup",
                    "title": question["question"],
                    "isRequired": True,
                    "choices": [
                    {
                        "text": answer["text"],
                        "correct": answer['correct'],
                        **({"value": answer['value']} if 'value' in answer else {})
                    }
                    for answer in question["answers"]
                ]
                }
                if 'name' in question:
                    question_data["name"] = question['name']
                else:
                    question_data["name"] = question["question"]
               
                json_data["pages"][0]["elements"].append(question_data)
        else:
            try:
                path = f"{LABS}/{course_name}/questions/" + question.get('figure')
                with open(path, 'r') as f:
                    markdown_file = f.read()
                markdown_content = markdown.markdown(markdown_file)
            except FileNotFoundError as e:
                flash(f"Figure File misnamed or missing, {e}", "error")
                return redirect(f"/result/{course_name}")
        
            if question['type'] == 'text':
                question_data =   {
                                    "type": "panel",
                                    "name": "figures_panel",
                                    "elements": [
                                        { "type": "html",
                                          "name": "code_example",
                                          "html": f"{markdown_content}"
                                        },
                                        {
                                          "type": "text",
                                          "name": f"{question['question']}",
                                          "title": question["question"],
                                          "isRequired": True,
                                          "value": question
                                        }
                                 ]}
                json_data["pages"][0]["elements"].append(question_data)
            else:
                question_data = {
                                "type": "panel",
                                "name": "figures_panel",
                                 "elements": [
                                    { "type": "html",
                                    "name": "code_example",
                                    "html": f"<div style='background-color: #C0C0E1; padding: 30px; color: black;'>{markdown_content}</div>"
                                    },
                                    {
                                    "type": "radiogroup",
                                    "name": f"{question['question']}",
                                    "title": question["question"],
                                    "isRequired": True,
                                    "choices": [
                                            {"text": answer["text"], 
                                             "correct": answer['correct']}for answer in question["answers"]
                                        ]
                                    }
                                 ]}
                json_data["pages"][0]["elements"].append(question_data)
        
    json_string = json.dumps(json_data, indent=4)

    # Create an in-memory file-like object
    file_stream = io.BytesIO(json_string.encode('utf-8'))

    # Create a response with the JSON content
    response = make_response(file_stream.getvalue())

    # Set the appropriate headers for JSON file download
    response.headers.set('Content-Type', 'application/json')
    response.headers.set('Content-Disposition', 'attachment', filename=file_name)

    return response


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=PORT, debug=True)
