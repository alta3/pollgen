# Use the official Python base image
FROM python:3.9-slim-buster

# Set the working directory inside the container
WORKDIR /app

# Copy the requirements.txt file into the container
COPY requirements.txt .

# Install the required dependencies
RUN pip install -r requirements.txt

# Copy the application code into the container
COPY . .

# Expose the port on which the Flask application runs
EXPOSE 2224

# Set the environment variables
ENV REPO_OWNER="csfeeser"
ENV REPO_NAME="pollgen"
ENV DIRECTORY_PATH="questions"
ENV FLASK_APP="pollgen.py"
ENV FLASK_RUN_HOST="0.0.0.0"
ENV FLASK_RUN_PORT="2224"
ENV FLASK_ENV="production"
ENV SECRET_KEY="any random string"
ENV LABS='labs/courses'
ENV DAY_ONE='labs/content/questions/common-day1.yml'
ENV PERFORMANCE='labs/content/questions/performance.yml'

# Start the Flask application
CMD ["flask", "run"]
