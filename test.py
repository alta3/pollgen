import hashlib

# Your input string
input_string = "Hello, World!"

# Create a hashlib object with SHA-256 hash function
hash_obj = hashlib.sha256()

# Update the hash object with your input string
hash_obj.update(input_string.encode('utf-8'))
print(hash_obj)
# Get the hexadecimal representation of the SHA-256 hash
hash_value = hash_obj.hexdigest()

# Print the SHA-256 hash value
print(hash_value)
