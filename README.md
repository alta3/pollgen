# PollGen

This is a simple tool for generating WebEx(.ATP) poll files. It allows you to create poll questions and choices, and then generates a WebEx-compatible poll file that can be imported into WebEx for conducting polls during meetings.

## Editing Existing Questions

All questions now exist in a stable location. You can find your questions in alta3/labs/courses/{COURSE_NAME}/questions/questions.yml If the lab does not have a questions folder it means that no questions have yet been made.

## Creating New Questions
If the course has no questions you will need to create a questions directory within /labs/courses/{course}/ and then create a questions.yml file within that directory. You can copy the structure and even copy the .yml file from labs/courses/template/questions/questions.yml. That .yml file contains examples and comments to ensure your questions and figures render properly. 
NOTE: When you create the questions directory and questions.yml file the course will automatically show up in the Pollgen drop-down. Within 3 minutes. 

All figures that you need can be made in a markdown (.md) file. Save them in the same /questions/ directory.
IMPORTANT NOTE:  Code Blocks must be separated from other text with a line at the top and the bottom. They must also all be idented one additional tab from the left (1 if there are none, 2 if there is 1, etc.)

## Installation

To install PollGen, follow these steps:

1. Start the container using the PollGen image:

    `sudo docker run -d -p 2224:2224 registry.gitlab.com/alta3/pollgen`
After downloading the repo from gitlab.com/alta3/pollgen be sure to install all required modules that you haven't yet. These are listed in requirement.txt

NOTES: You will need to store environment variables for LABS, DAY_ONE, and PERFORMANCE path structures.
       This code expects a directory structure that follows /course/{course_name}/questions/questions.yml.
       LABS is the environment variable where you define all directory structure preceding and including /course.
       DAY_ONE is an environment variable point to a specific .yml containing non-course-specific Day 1 introductory questions.
       PERFORMANCE is an environment variable point to a specific .yml containing non-course-specific course performance questions.


To start a local image of Pollgen to tinker with you can simply run the pollgen.py script from within the directory.
    ```python
    python3 pollgen.py
    ```

1. Once you have started the PollGen script, open your preferred web browser to the URL [http://localhost:2224](http://localhost:2224) to access the PollGen user interface **OR Aux1 if using an Alta3 lab environment.**

2. Follow the on-screen instructions to create your poll questions and choices. You can check the questions you want to include and click "Submit" to generate the WebEx poll file.

3. After generating the poll file, you can import it into WebEx and use it during your meetings to conduct polls.

## Contributing

If you would like to contribute to PollGen, please follow these steps:

1. Fork the PollGen repository on GitLab.

2. Create a new branch for your feature or bug fix.

3. Make the necessary changes and commit your code.

4. Push your changes to your forked repository.

5. Submit a pull request to the main PollGen repository.

6. Your contribution will be reviewed, and if accepted, it will be merged into the main repository.

## Issues and Feedback

If you encounter any issues while using PollGen or have any feedback or suggestions for improvement, please [open an issue](https://gitlab.com/alta3/pollgen/issues) on the GitLab repository. Your feedback is valuable and greatly appreciated.

## Credits
If you encounter any issues while using PollGen or have any feedback or suggestions for improvement, please [open an issue](https://gitlab.com/alta3/pollgen/issues) on the GitLab repository. Your feedback is valuable and greatly appreciated.

PollGen was created by Chad Feeser. It is an open-source project released under the MIT License. The project repository can be found on GitLab: [https://gitlab.com/alta3/pollgen](https://gitlab.com/alta3/pollgen)

## Disclaimer

Please note that PollGen is provided as-is and is not affiliated with or endorsed by WebEx or any other third-party service. It is your responsibility to ensure compliance with any applicable terms of service or usage policies when using PollGen and the generated poll files.

## Contact

For any inquiries or further information about PollGen, please contact [jspizzandre@alta3.com](mailto:jspizzandre@alta3.com).

Thank you for using PollGen! We hope it enhances your WebEx meeting experience.
