# pollgen testing (defensive coding 101)

### Testing done on local machine

- [ ] Test for missing questions.yml
- [ ] Test for defective questions.yml
- [ ] Test for missing figure file
- [ ] Run against course that does not work
- [ ] Run against course that works
- [ ] Test size limit of questions.yml


1. Test for missing questions.yml

```
clone github/labs repo
clone gitlab/pollgen
run pollgen locally
try pyb
verify working
cd labs/courses/pyb
mv questions.yml break.questions.yml
try pyb
verify broken
mv break.questions.yml questions.yml
```

2. Test for defective questions.yml (make yml file syntactically incorrect)

```
vi questions.yml
#make a question that is incorrect format / syntax
run pollgen locally
try pyb
verify error
```

3. Test for missing figure file

```
run pollgen locally
try pyb
verify working
remove figure from a question in pyb
try pyb and select question that has missing figure file
verify broken

```

4. Test size limit of questions.yml (test on course with largest questions.yml)

```
Make pyb questions.yml 4x in size (to make it larger than 4kb) testing session size
run pollgen locally
```
